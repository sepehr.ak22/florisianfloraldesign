from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    """Model definition for Post."""

    title = models.CharField(max_length = 100)
    slug = models.SlugField(max_length = 110)
    summary = models.CharField(max_length = 255)
    content = models.TextField()
    author = models.ForeignKey(User, related_name='posts', on_delete=models.DO_NOTHING)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)


    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):       
       super(Post, self).save(*args, **kwargs) # Call the real save() method


class Tag(models.Model):
    title = models.CharField(max_length = 100)
    slug = models.SlugField(max_length = 110)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):       
       super(Tag, self).save(*args, **kwargs)