from django.urls import path, re_path, include
from . import views

urlpatterns = [
    re_path(r'^$', views.HomeView.as_view(), name='home'),
    re_path(r'^gallery/$', views.GalleryView.as_view(), name='gallery'),
    re_path(r'^posts/$', views.PostsView.as_view(), name='posts'),
    re_path(r'^faq/$', views.FAQView.as_view(), name='faq'),
    re_path(r'^sayhello/$', views.ContactView.as_view(), name='contact'),
    re_path(r'^story/$', views.StoryView.as_view(), name='story'),
]
