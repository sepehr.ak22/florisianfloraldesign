from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.utils.text import slugify

from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField


class Site(models.Model):
    full_name = models.CharField(_("Owner Name"), max_length=200, default = "Frida Jafari")
    name = models.CharField(_("Website Name"), max_length=200)
    domain = models.URLField(_("Domain"), max_length=200)
    footer = models.TextField()
    address = models.CharField(_("Address"), max_length = 254)
    phone_number = models.CharField(_("Phone Number"), max_length = 15, help_text = "+98912...")
    about_me = RichTextField(_("About Me"), help_text = "This content shows on my Resume section")

    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(upload_to='site/banners', height_field='height_field', width_field='width_field', max_length=100, verbose_name=_("About Me Picture"), null = True)
    
    insta = models.URLField(_("Instagram"), max_length=200)
    facebook = models.URLField(_("Facebook"), max_length=200)
    mail = models.EmailField(_("Mail"), max_length=200)
    
    logo = models.FileField(upload_to='site/',  max_length=100, validators=[FileExtensionValidator(['svg'])])

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    def __str__(self):
        return self.name

class PageDetail(models.Model):
    name = models.CharField(_("Page Name"), max_length=50)
    description = models.CharField(_("Description"), max_length=254)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

class Keyword(models.Model):
    name = models.CharField(_("Keyword Name"), max_length=50)
    page = models.ForeignKey('PageDetail', related_name='keywords', on_delete=models.CASCADE)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

class FAQ(models.Model):
    question = models.CharField(_("Question"), max_length=254)
    answer = models.CharField(_("Answer"), max_length=254)
    priority = models.PositiveSmallIntegerField(unique = True, help_text = 'The order in which the question and answer are displayed')
    site = models.ForeignKey('Site', related_name='faq', on_delete=models.CASCADE)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = 'FAQ'
        verbose_name_plural = 'FAQ'
        ordering = ('priority',)

class Page(models.Model):

    title = models.CharField(max_length = 20)
    keywords = models.CharField(max_length = 255)
    description = models.CharField(max_length = 255)

    class Meta:
        """Meta definition for PageTemplate."""

        verbose_name = 'PageTemplate'
        verbose_name_plural = 'PageTemplates'

    def __str__(self):
        return self.title

class Banner(models.Model):
    AVAILABLE_URLS = (
        ('home', 'Home'),
        ('gallery', 'Gallery'),
        ('posts', 'Posts'),
        ('faq', 'FAQ'),
        ('story', 'Our Story'),
        ('contact', 'Say Hello'),
    )

    text = models.CharField(_("Text"), max_length = 30)

    width_field = models.PositiveIntegerField(_("Width Field"), editable = False)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False)
    pic = models.ImageField(upload_to='site/banners', height_field='height_field', width_field='width_field', max_length=100, verbose_name=_("Banner"))

    priority = models.PositiveIntegerField(_("Priority"), help_text="Priority of showing banners")
    
    url_name = models.CharField(_("Related URL Name"), choices=AVAILABLE_URLS, max_length = 15, blank = True, null = True, help_text = "leave this field if it is main banner")

    is_main = models.BooleanField(_("Big Banner"), default = True, help_text="check this if you want to show the picture bellow the banner")

    site = models.ForeignKey('Site', related_name='banners', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
       small_banners = Banner.objects.filter(is_main = False)
       if len(small_banners) > 4:
           raise ValidationError('We have enough small banners. please change them')
       super(Banner, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name = _('Banner')
        verbose_name_plural = _('Banners')
        ordering = ('priority',)

    def __str__(self):
        return self.text

class InstagramPosts(models.Model):
    text = models.CharField(max_length = 30)
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False)
    pic = models.ImageField(upload_to='site/insta', height_field='height_field', width_field='width_field', max_length=100)
    likes = models.PositiveSmallIntegerField(_("Likes"))
    comments = models.PositiveSmallIntegerField(_("Comments"))
    site = models.ForeignKey('Site', related_name='insta_posts', on_delete=models.CASCADE)


    class Meta:
        verbose_name = 'Instagram Post'
        verbose_name_plural = 'Instagram Posts'

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):   
       super(InstagramPosts, self).save(*args, **kwargs)

class Category(models.Model):

    title = models.CharField(_('Title'), max_length = 50)
    slug = models.SlugField(_("Slug"), max_length = 60, editable = False)
    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)



    class Meta:
        """Meta definition for Category."""

        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(Category, self).save(*args, **kwargs)

class Blog(models.Model):
    title = models.CharField(_("Title"), max_length = 120)
    author = models.ForeignKey(User, verbose_name=_("Author"), on_delete=models.CASCADE)
    summary = models.CharField(_("Summary"), max_length = 255)
    content = models.TextField(_("Content"))
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False)
    pic = models.ImageField(upload_to='site/insta', height_field='height_field', width_field='width_field', max_length=100)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       super(Blog, self).save(*args, **kwargs)

class Tag(models.Model):
    
    title = models.CharField(_("Title"), max_length = 120)
    slug = models.SlugField(_("Slug"))
    post = models.ForeignKey("Blog", related_name='tags', verbose_name=_("Post"), on_delete=models.CASCADE)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       super(Tag, self).save(*args, **kwargs)

class Group(models.Model):
    group = models.PositiveSmallIntegerField(_("Group triple pictures"))    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        """Meta definition for Group."""

        verbose_name = _('Group')
        verbose_name_plural = _('Groups')

    def __str__(self):
        return str(self.group)

    def save(self, *args, **kwargs):
       super(Group, self).save(*args, **kwargs)

class Gallery(models.Model):

    text = models.CharField(max_length = 80)
    alternate_text = models.CharField(max_length = 80)
    
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False)
    pic = models.ImageField(upload_to='site/gallery', height_field='height_field', width_field='width_field', max_length=100)

    width_field_thumb = models.PositiveIntegerField(_("Width Field Thumbnail"), editable = False)
    height_field_thumb = models.PositiveIntegerField(_("Height Field Thumbnail"), editable = False)
    thumb = models.ImageField(upload_to='site/gallery', height_field='height_field_thumb', width_field='width_field_thumb', max_length=100)

    group = models.ForeignKey('Group', related_name='galleries', on_delete=models.CASCADE)
    category = models.ForeignKey('Category', related_name='galleries', on_delete=models.CASCADE)
    site = models.ForeignKey('Site', related_name='galleries', on_delete=models.CASCADE)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        """Meta definition for Gallery."""

        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')

    def __str__(self):
        """Unicode representation of Gallery."""
        return self.text

    def save(self, *args, **kwargs):
       super(Gallery, self).save(*args, **kwargs)

class Testimonial(models.Model):
    """Model definition for Testimonial."""

    full_name = models.CharField(max_length = 100)
    email = models.EmailField(max_length = 100)
    content = models.CharField(max_length = 255)
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(upload_to='site/gallery', height_field='height_field', width_field='width_field', max_length=100, null = True)
    is_shown = models.BooleanField(default = False)
    priority = models.SmallIntegerField(unique = True, null = False, blank = False)
    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    
    class Meta:
        """Meta definition for Testimonial."""

        verbose_name = 'Testimonial'
        verbose_name_plural = 'Testimonials'

    def __str__(self):
        """Unicode representation of Testimonial."""
        return self.full_name

    def save(self, *args, **kwargs):
       
       super(Testimonial, self).save(*args, **kwargs)

class Contact(models.Model):
    SERVICES = (
        ("c", 'Ceremony Florals'),
        ("r", 'Reception Florals'),
        ("b", 'Bridal Party'),
        ("o", 'Other'),
    )

    SOCIAL_MEDIAS = (
        ("g", "Google Search"),
        ("f", "Facebook"),
        ("i", "Instagram"),
        ("w", "Words of mouth"),
        ("a", "Attend Event"),
    )
    full_name = models.CharField(_("Name"), max_length = 100)
    email = models.EmailField(_("Email"), max_length = 100)
    wedding_date = models.DateField(_("Date of Wedding or Event"))
    phone_number = models.CharField(_("Phone Number"), max_length = 12)
    services = models.CharField(_("Services"), choices = SERVICES, max_length = 1)
    from_where = models.CharField(_("Services"), choices = SOCIAL_MEDIAS, max_length = 1)
    subject = models.CharField(_("Subject"), max_length = 100)
    message = models.TextField(_("Message"))

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        """Meta definition for Contact."""

        verbose_name = 'Contact'
        verbose_name_plural = 'Contacts'
        ordering = ('-created',)

    def __str__(self):
        """Unicode representation of Contact."""
        return self.full_name

    def save(self, *args, **kwargs):
       
       super(Contact, self).save(*args, **kwargs)