from django import forms
from .models import Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = [
            'full_name',
            'email',
            'wedding_date',
            'services',
            'phone_number',
            'from_where',
            'subject',
            'message'
        ]

class TestimonialsForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = [
            'full_name',
            'email',
            'subject',
            'message'
        ]
