# Generated by Django 2.2.10 on 2020-04-19 05:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0023_auto_20200402_1915'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group', models.PositiveSmallIntegerField(verbose_name='Group triple pictures')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
            ],
            options={
                'verbose_name': 'Group',
                'verbose_name_plural': 'Groups',
            },
        ),
        migrations.AddField(
            model_name='gallery',
            name='alternate_text',
            field=models.CharField(default='seo', max_length=80),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gallery',
            name='height_field_thumb',
            field=models.PositiveIntegerField(default=350, editable=False, verbose_name='Height Field Thumbnail'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gallery',
            name='thumb',
            field=models.ImageField(default='assets/img/7.jpg', height_field='height_field_thumb', upload_to='site/gallery', width_field='width_field_thumb'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gallery',
            name='width_field_thumb',
            field=models.PositiveIntegerField(default=350, editable=False, verbose_name='Width Field Thumbnail'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gallery',
            name='group',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='galleries', to='pages.Group'),
            preserve_default=False,
        ),
    ]
