# Generated by Django 3.0 on 2020-03-14 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0013_blog_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='about_me',
            field=models.TextField(default='lorem ipsum', help_text='This content shows on my story page', verbose_name='About Me'),
            preserve_default=False,
        ),
    ]
