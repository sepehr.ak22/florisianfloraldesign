# Generated by Django 3.0 on 2020-04-03 01:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0018_auto_20200329_0457'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='url_name',
            field=models.CharField(blank=True, help_text='leave this field if it is main banner', max_length=15, null=True, verbose_name='Related URL Name'),
        ),
        migrations.AlterField(
            model_name='site',
            name='about_me',
            field=models.TextField(help_text='This content shows on my Resume section', verbose_name='About Me'),
        ),
    ]
