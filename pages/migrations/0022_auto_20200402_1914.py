# Generated by Django 3.0 on 2020-04-03 02:14

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0021_site_full_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='phone_number',
            field=models.CharField(default='00000000000', max_length=12, verbose_name='Phone Number'),
        ),
        migrations.AlterField(
            model_name='site',
            name='about_me',
            field=ckeditor.fields.RichTextField(help_text='This content shows on my Resume section', verbose_name='About Me'),
        ),
    ]
