from django.contrib import admin

from .models import Banner
from .models import Page
from .models import InstagramPosts
from .models import Site
from .models import Gallery
from .models import Group
from .models import Category
from .models import FAQ
from .models import Contact
from .models import Blog
from .models import Testimonial

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug')

@admin.register(Blog)
class PostsAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'created',)

@admin.register(Group)
class PostsAdmin(admin.ModelAdmin):
    list_display = ('group', 'created',)

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('subject', 'full_name', 'created',)

@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    list_display = ('question', 'priority', 'modified', 'created')

@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ('text', 'category', 'group',)
    list_editable = ('category', 'group')
    list_select_related = ('group',)

@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('title',)

@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ('text', 'is_main', 'width_field', 'height_field', )
    list_filter = ('is_main',)

@admin.register(InstagramPosts)
class InstagramPostsAdmin(admin.ModelAdmin):
    
    list_display = ('text', 'likes', 'comments')

    def has_add_permission(self, request):
        posts = InstagramPosts.objects.all()
        if len(posts) <= 4:
            return True
        return False

@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    '''Admin View for Site'''

    list_display = ('domain', 'insta', 'mail', 'facebook')
    
    def has_add_permission(self, request):
        site = Site.objects.all()
        if len(site) <= 1:
            return True
        return False

@admin.register(Testimonial)
class TestimonialAdmin(admin.ModelAdmin):

    list_display = ('full_name', 'priority', 'created')