from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, View, CreateView


from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

from .forms import ContactForm

from .models import Site
from .models import Blog
from .models import Category
from .models import Contact
from .models import Testimonial
from .models import Group
from .models import FAQ

class HomeView(TemplateView):
    template_name = "pages/index.html"
    page_name = 'Home'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()

        return context

class PostsView(TemplateView):
    template_name = "pages/posts.html"
    page_name = 'Posts'

    def get_context_data(self, **kwargs):
        context = super(PostsView, self).get_context_data(**kwargs)
        context['posts'] = Blog.objects.all()
        context['site'] = Site.objects.first()

        return context

class StoryView(TemplateView):
    template_name = "pages/story.html"
    page_name = 'Story'

    def get_context_data(self, **kwargs):
        context = super(StoryView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['testimonial'] = Testimonial.objects.filter(is_shown = True).order_by('priority')[:5]
        return context

class ContactView(SuccessMessageMixin, CreateView):
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('contact')
    success_message = "Your message has been sent to us. Thanks for your consideration. we will in touch as soon as possible"
    template_name = 'pages/contact.html'
    
    page_name = 'Say Hello'

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()

        return context

class FAQView(TemplateView):
    template_name = "pages/faq.html"
    page_name = 'FAQ'

    def get_context_data(self, **kwargs):
        context = super(FAQView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['faqs'] = FAQ.objects.all()

        return context

class GalleryView(TemplateView):
    template_name = "pages/gallery.html"
    page_name = 'Gallery'

    def get_context_data(self, **kwargs):
        context = super(GalleryView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['categories'] = Category.objects.all()
        context['groups'] = Group.objects.all()

        return context
